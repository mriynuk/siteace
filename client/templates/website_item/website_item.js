Template.website_item.events({
	'click .js-upvote': function(event) {
		if (!Meteor.user()) {
			return false;
		};
		// example of how you can access the id for the website in the database
		// (this is the data context for the template)
		var website_id,
			vote;

		website_id = this._id;
		vote = Votes.findOne({createdBy: Meteor.user()._id, site: website_id, vote: 1});
		website = Websites.findOne({"_id": website_id});

		// put the code in here to add a vote to a website!
		if (!vote) {
			Votes.insert({
				site: website_id,
				vote: 1,
				createdBy: Meteor.user()._id,
				createdOn: new Date(),
			});
		    Websites.update({_id: website_id},
		        {$set: {voted_up: parseInt(website.voted_up) + 1}}
		    );
		}
		else {
			Votes.remove(vote._id);
		    Websites.update({_id: website_id},
		        {$set: {voted_up: parseInt(website.voted_up) - 1}}
		    );
		}

		return false; // prevent the button from reloading the page
	}, 
	'click .js-downvote': function(event) {
		if (!Meteor.user()) {
			return false;
		};
		// example of how you can access the id for the website in the database
		// (this is the data context for the template)
		var website_id,
			vote;

		website_id = this._id;
		vote = Votes.findOne({createdBy: Meteor.user()._id, site: website_id, vote: -1});
		website = Websites.findOne({"_id": website_id});

		// put the code in here to remove a vote from a website!
		if (!vote) {
			Votes.insert({
				site: website_id,
				vote: -1,
				createdBy: Meteor.user()._id,
				createdOn: new Date(),
			});
		    Websites.update({_id: website_id},
		        {$set: {voted_down: parseInt(website.voted_down) + 1}}
		    );
		}
		else {
			Votes.remove(vote._id);
		    Websites.update({_id: website_id},
		        {$set: {voted_down: parseInt(website.voted_down) - 1}}
		    );
		}

		return false; // prevent the button from reloading the page
	}
})
