Template.comment_form.events({
  'submit .js-save-comment-form': function(event) {
    // here is an example of how to get the url out of the form:
    if (Meteor.user()) {
      var body = event.target.body.value;

      if (body) {
        Comments.insert({
          website: this.website_id,
          body: body,
          createdOn: new Date(),
          createdBy: Meteor.user()._id
        });
      };
    }

    $(event.target).trigger('reset');
    $(event.target).closest('.modal').modal('hide');

    return false;
  }
});