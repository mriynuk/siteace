Template.comment_item.helpers({
  getUser: function (user_id) {
    return Meteor.users.findOne({"_id": user_id}) ? Meteor.users.findOne({"_id": user_id}).username : "anonymous";
  },
});
