// Set up security on Images collection.
Websites.allow({
  insert: function(userId, doc) {
    if (Meteor.user() && userId == doc.createdBy) { // they are logged in.
      return true;
    }
    else { // user not logged in
      return false;
    }
  },
  update: function(userId, doc) {
    if (Meteor.user()) { // they are logged in.
      return true;
    }
    else {
      return false;
    }
  },
  remove: function(userId, doc) {
    if (Meteor.user() && userId == doc.createdBy) { // they are logged in.
      return true;
    }
    else {
      return false;
    }
  },
});

Votes.allow({
  insert: function(userId, doc) {
    if (Meteor.user() && userId == doc.createdBy) { // they are logged in.
      return true;
    }
    else { // user not logged in
      return false;
    }
  },
  remove: function(userId, doc) {
    if (Meteor.user() && userId == doc.createdBy) { // they are logged in.
      return true;
    }
    else {
      return false;
    }
  },
});

Comments.allow({
  insert: function(userId, doc) {
    if (Meteor.user() && userId == doc.createdBy) { // they are logged in.
      return true;
    }
    else { // user not logged in
      return false;
    }
  },
  remove: function(userId, doc) {
    if (Meteor.user() && userId == doc.createdBy) { // they are logged in.
      return true;
    }
    else {
      return false;
    }
  },
});
