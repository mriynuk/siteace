// Routing.

Router.configure({
  layoutTemplate:'ApplicationLayout'
});

Router.route('home', {
  path: '/',
  //layoutTemplate: 'ApplicationLayout',
  yieldTemplates: {
    'navbar': {to: 'navbar'},
    'website_form': {to: 'form'},
    'website_list': {to: 'main'}
  }
});

Router.route('/website/:_id', function () {
  this.render('navbar', { to:"navbar" });
  this.render('website_item', {
    to:"form", 
    data:function(){
      return Websites.findOne({_id: this.params._id});
    }
  }),
  this.render('comment_list', {
    to:"main", 
    data:function(){
      return {website_id: this.params._id, comments: Comments.find({website: this.params._id})};
    }
  });
}, {name: 'website'});
